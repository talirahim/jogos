<?php


namespace routes;

use Slim\App;
use src\App\Controllers\AuthController as AuthController;
use src\App\Controllers\SiteController as SiteController;
use src\App\Controllers\CompeticaoController as CompeticaoController;
use src\App\Controllers\ResultadoController as ResultadoController;
use src\Domain\Models\Competicao;
use src\Domain\Models\Resultado;
use src\Domain\Service\CompeticaoService;
use src\App\Middlewares\jwtAuth;
use \Firebase\JWT\JWT;

$app = new App(
    ['settings' => [
        'displayErrorDetails' => 'true'
    ]]
);

$app->post('/login', AuthController::class . ':login');

$app->group('/api', function(App $app) {
    $app->post('/criar', CompeticaoController::class . ':createCompeticao');
    $app->post('/finalizar/{competicao_id}', CompeticaoController::class . ':finalCompeticao');
    $app->get('/ranking/{competicao_id}', ResultadoController::class . ':getRanking');
    $app->post('/resultado/{competicao_id}', ResultadoController::class . ':createResultado');
});