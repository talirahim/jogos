<?php


namespace src\App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use src\Domain\Models\Competicao;
use src\Domain\Models\User;
use src\Domain\Service\LoginService;
use \Firebase\JWT\JWT;



class AuthController

{

    public function login(Request $req, Response $res, $args)
    {
        $data = $req->getParsedBody();

        $user = User::where('email', $data['email'])->first();

        if(!$user) {
            return $res->withJson(['error' => true, 'message' => 'Email não cadastrado.']);
        }

        if($data['password'] != $user->password){
            return $res->withJson(['error' => true, 'message' => 'Senha incorreta.']);
        }

        $token = JWT::encode(['email' => $user->email, 'password' => $user->password], getenv("JWT_SECRET_KEY"), "HS256");

        return $res->withJson(['token' => $token]);

    }

}