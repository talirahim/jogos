<?php

namespace src\App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use src\Domain\Models\Resultado;
use src\Domain\Models\Competicao;
use src\Domain\Service\ResultadoService;

class ResultadoController
{
    public function createResultado(Request $req, Response $res, $args)
    {
        $id = $args['competicao_id'];

        $data = $req->getParsedBody();

        // lista parametros que sao obrigatorios
        $mandatory_params = ['value', 'unidade', 'atleta_id'];

        foreach ($mandatory_params as $mandatory_param) {
            if (!$data[$mandatory_param]) { // se o parametro obrigatorio nao eh colocado
                return $res->withJson([
                    "Error" => "Parameter `{$mandatory_param}` should not be empty"
                ]);
            }
        }

        $checkCompeticao = Competicao::where('id', '=', $id)->first();

        if (!empty($checkCompeticao->fim)) {
            return $res->withJson([
                "Error" => "Competição já foi finalizada, não é possível inserir resultado"
            ]);
        }

        $competicao = Competicao::where('id', $id)->first();

        $ranking = Resultado::where('competicao_id', $id)
            ->select('atleta_id')
            ->get();

        $i = 0;

        foreach ($ranking as $rank) {
            if ($rank->atleta_id == $data['atleta_id'] && $competicao->modalidade_id == 1) { /*100m rasos */
                return $res->withJson([
                    "Error" => "Atleta da modalidade 100m rasos já possui resultado"
                ]);
            } else {
                if ($rank->atleta_id == $data['atleta_id'] && $competicao->modalidade_id == 2) { /*Dardos */
                    $i = $i + 1;
                }
                if ($i == 3){
                    return $res->withJson([
                        "Error" => "Atleta da modalidade Dardos já possui resultado, após 3 tentativas"
                    ]);
                }
            }
        }

        ResultadoService::newResult($data, $id);

        return $res->withJson([
            "Resultado de competição inserido com sucesso!"
        ]);

    }

    public function getRanking(Request $req, Response $res, $args)
    {
        $id = $args['competicao_id'];

        $ranking = ResultadoService::newRanking($id);

        return $res->withJson([
            "Ranking" => $ranking
        ]);

    }
}