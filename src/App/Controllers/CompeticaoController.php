<?php

namespace src\App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use src\Domain\Models\Competicao;
use src\Domain\Models\Resultado;
use src\Domain\Models\Aleta;
use src\Domain\Service\CompeticaoService;
use src\Domain\Service\ResultadoService;

class CompeticaoController
{

    public function createCompeticao(Request $req, Response $res, $args)
    {

        $data = $req->getParsedBody();

        // lista parametros que sao obrigatorios
        $mandatory_params = ['nome', 'inicio', 'modalidade_id'];

        foreach ($mandatory_params as $mandatory_param) {
            if (!$data[$mandatory_param]) { // se o parametro obrigatorio nao eh colocado
                return $res->withJson([
                    "Error" => "Parâmetro `{$mandatory_param}` não pode estar vazio"
                ]);
            }
        }

        $competicao = CompeticaoService::newCompetition($data);

        $modalidade = $competicao->modalidade()->first();
        $nome = $modalidade->nome;

        return $res->withJson([
            "message" => "Competição iniciada com sucesso!",
            "id" => "$competicao->id",
            "nome" => "$competicao->nome",
            "inicio" => "$competicao->inicio",
            "modalidade" => "$nome"
        ]);
    }

    public function finalCompeticao(Request $req, Response $res, $args)
    {
        $id = $args['competicao_id'];
        $data = $req->getParsedBody();

        $mandatory_params = ['fim'];

        foreach ($mandatory_params as $mandatory_param) {
            if (!$data[$mandatory_param]) { // se o parametro obrigatorio nao é colocado
                return $res->withJson([
                    "Error" => "Parâmetro `{$mandatory_param}` não pode estar vazio"
                ]);
            }
        }

        $checkCompeticao = Competicao::where('id', $id)->first();

        if (!empty($checkCompeticao->fim)) {
            return $res->withJson([
                "Error" => "Competição já foi finalizada"
            ]);
        }

        $competicao = CompeticaoService::finalCompetition($data, $id);

        return $res->withJson([
            "Competição finalizada!"
        ]);

    }

}

