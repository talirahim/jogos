<?php

namespace src\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Resultado extends Model
{
    protected $table = 'resultado';

    public $timestamps = false;

    protected $fillable = ['value', 'unidade', 'competicao_id', 'atleta_id'];

    protected $primaryKey = 'id';

    public function competicoes()
    {
        return $this->hasOne('Competicao');
    }

    public function atleta()
    {
        return $this->belongsTo(Atleta::class, 'atleta_id', 'id');
    }

}
