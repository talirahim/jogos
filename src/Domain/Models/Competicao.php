<?php

namespace src\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Competicao extends Model
{
    protected $table = 'competicoes';

    public $timestamps = false;

    protected $fillable = ['nome', 'inicio', 'fim', 'modalidade_id'];

    protected $primaryKey = 'id';

    public function resultado()
    {
        return $this->hasMany('Resultado');
    }

    public function modalidade()
    {
        return $this->belongsTo(Modalidade::class, 'modalidade_id', 'id');
    }
}
