<?php

namespace src\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Modalidade extends Model
{
    protected $table = 'modalidade';

    public $timestamps = false;

    protected $fillable = ['nome'];

    protected $primaryKey = 'id';
}