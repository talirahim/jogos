<?php

namespace src\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Atleta extends Model
{
    protected $table = 'atleta';

    public $timestamps = false;

    protected $fillable = ['nome'];

    protected $primaryKey = 'id';

    public function resultado()
    {
        return $this->hasMany('Resultado', 'atleta_id', 'id');
    }
}