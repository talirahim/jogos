<?php

namespace src\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';

    public $timestamps = false;

    protected $fillable = ['nome', 'email', 'password', 'token'];

    protected $primaryKey = 'id';
}