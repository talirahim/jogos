<?php

namespace src\Domain\Service;

use Slim\Http\Request;
use Slim\Http\Response;
use src\Domain\Models\Competicao;
use src\Domain\Models\Resultado;

class ResultadoService
{
    public static function newResult($data, $id){

        $resultado = new Resultado();

        $resultado->value = $data['value'];
        $resultado->unidade = $data['unidade'];
        $resultado->competicao_id = $id;
        $resultado->atleta_id = $data['atleta_id'];
        $resultado->save();

    }

    public static function newRanking($id)
    {

        $posicao = 0;

        $competicao = Competicao::where('id', $id)->first();

        if ($competicao->modalidade_id == 1) { /*100m rasos - menor tempo vence */

            $ranking = Resultado::where('competicao_id', $id)
                ->orderBy('value', 'asc')
                ->select('atleta_id', 'value')
                ->get();

            $ranking_arr = array();

            foreach ($ranking as $rank) {

                $atleta = $rank->atleta()->first();
                $nome = $atleta->nome;

                array_push($ranking_arr, [

                    "Atleta" => $nome,
                    "Value" => $rank->value,
                    "Posicao" => $posicao = $posicao + 1
                ]);
            }

        } else { /* Lançamento dardo - maior distancia vence */

            $ranking = Resultado::select('atleta_id',Resultado::raw('max(value) as value'))
                ->where('competicao_id', $id)
                ->orderBy('value', 'desc')
                ->groupBy('atleta_id')
                ->get();

            $ranking_arr = array();

            foreach ($ranking as $rank) {

                $atleta = $rank->atleta()->first();
                $nome = $atleta->nome;

                array_push($ranking_arr, [

                    "Atleta" => $nome,
                    "Value" => $rank->value,
                    "Posicao" => $posicao = $posicao + 1

                ]);
            }
        }
        return $ranking_arr;
    }
}