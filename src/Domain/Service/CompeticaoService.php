<?php

namespace src\Domain\Service;

use src\Domain\Models\Competicao;

class CompeticaoService
{
    public static function newCompetition($data){

        $competicao = new Competicao();

        $competicao->nome = $data['nome'];
        $competicao->inicio = $data['inicio'];
        $competicao->modalidade_id = $data['modalidade_id'];
        $competicao->save();

        return $competicao;

    }

    public static function finalCompetition($data, $id)
    {
        $competicao = Competicao::where('id', '=', $id)->first();

        if(!empty($competicao)){
            $competicao->fim = $data['fim'];
            $competicao->save();
        }

        return $competicao;
    }

}