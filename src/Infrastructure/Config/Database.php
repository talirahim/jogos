<?php

namespace src\Infrastructure;

use Illuminate\Database\Capsule\Manager as Capsule;

class Database
{
    public static function conectar()
    {
        $capsule = new Capsule;
        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => getenv('DB_HOST'),
            'database'  => getenv('DB_NAME'),
            'username'  => getenv('DB_USER'),
            'password'  => getenv('DB_PASS'),
            'port' => '3308',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();

    }

    public function slimConfiguration(): \Slim\Container
    {
        $configuration = [
            'settings' => [
                'displayErrorDetails' => getenv('DISPLAY_ERRORS_DETAILS'),
                'db' => [
                    'driver' => 'mysql',
                    'host' => getenv('DB_HOST'),
                    'database' => getenv('DB_NAME'),
                    'username' => getenv('DB_USER'),
                    'password' => getenv('DB_PASS'),
                    'port' => '3306',
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                ]
            ],
        ];
        return new \Slim\Container($configuration);
    }
}
