<?php


use Phinx\Seed\AbstractSeed;

class Atleta extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'nome' => 'Anderson Lima'
            ],
            [
                'nome' => 'Marcio dos Santos'
            ],
            [
                'nome' => 'Joaquim Pereira'
            ],
            [
                'nome' => 'Vitória Matos'
            ]
        ];

        $task = $this->table('atleta');
        $task->insert($data)
             ->save();
    }
}
