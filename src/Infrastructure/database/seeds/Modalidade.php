<?php


use Phinx\Seed\AbstractSeed;

class Modalidade extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'nome' => '100m rasos'
            ],
            [
                'nome' => 'Lançamento Dardo'
            ]
        ];

        $task = $this->table('modalidade');
        $task->insert($data)
            ->save();
    }
}
