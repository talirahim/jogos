<?php

use Phinx\Seed\AbstractSeed;

class Competicoes extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'nome' => 'Atletismo',
                'inicio' => '2020-08-30 08:00:00',
                'fim' => '2020-08-30 10:00:00',
                'modalidade_id' => 1,
                'atleta_id' => 1
            ]
        ];

        $task = $this->table('competicoes');
        $task->insert($data)
             ->save();
    }
}
