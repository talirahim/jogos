<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddFK extends AbstractMigration
{
    public function change(): void
    {
        $refTable = $this->table('competicoes');
        $refTable->addColumn('modalidade_id', integer, ['null' => true])
                 ->addForeignKey('modalidade_id', 'modalidade', 'id')
                 ->save();
    }
}
