<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TableModalidade extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('modalidade');
        $table->addColumn('nome', string)
              ->create();
    }
}
