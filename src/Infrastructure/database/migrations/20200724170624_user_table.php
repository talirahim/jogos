<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UserTable extends AbstractMigration
{

    public function change(): void
    {
        $table = $this->table('user');
        $table->addColumn('nome', string)
              ->addColumn('email', string)
              ->addColumn('password', string)
              ->create();
    }
}
