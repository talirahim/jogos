<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UserEmail extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('user');
        $table->addIndex(['email'], [
                          'unique' => true,
                          'name' => 'idx_users_email'])
              ->save();
    }
}
