<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TableAtleta extends AbstractMigration
{

    public function change(): void
    {
        $table = $this->table('atleta');
        $table->addColumn('nome', string)
              ->create();
    }
}
