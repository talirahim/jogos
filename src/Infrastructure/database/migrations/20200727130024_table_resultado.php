<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TableResultado extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('resultado');
        $table->addColumn('value', decimal, ['null' => false])
              ->addColumn('unidade', string, ['null' => false])
              ->create();


        $refTable = $this->table ( 'resultado' );
        $refTable->addColumn ( 'competicao_id' ,  'integer' ,  [ 'null'  =>  false ])
            ->addColumn ( 'atleta_id' ,  'integer' ,  [ 'null'  =>  false ])
            ->addForeignKey ( 'competicao_id' ,  'competicoes' ,  'id')
            ->addForeignKey ( 'atleta_id' ,      'atleta' ,  'id')
            -> save();

    }
}
