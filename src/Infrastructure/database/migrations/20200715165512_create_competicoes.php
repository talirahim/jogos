<?php


use Phinx\Migration\AbstractMigration;

final class CreateCompeticoes extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('competicoes');
        $table->addColumn('nome', string)
              ->addColumn('inicio', datetime, ['null' => true])
              ->addColumn('fim', datetime, ['null' => true])
        ->create();

    }
}
