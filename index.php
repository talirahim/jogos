<?php

//use Illuminate\Database\Capsule\Manager as Capsule;
require_once './vendor/autoload.php';
require_once './.env';
require_once './routes/Api.php';
require_once './src/App/Controllers/AuthController.php';
require_once './src/App/Controllers/CompeticaoController.php';
require_once './src/App/Controllers/ResultadoController.php';
require_once './src/Domain/Models/Competicao.php';
require_once './src/Domain/Models/Atleta.php';
require_once './src/Domain/Models/Modalidade.php';
require_once './src/Domain/Models/Resultado.php';
require_once './src/Domain/Models/User.php';
require_once './src/Domain/Service/CompeticaoService.php';
require_once './src/Infrastructure/Config/Database.php';
require_once './src/App/Middlewares/jwtAuth.php';

use src\App\Controllers\CompeticaoController;

use src\Infrastructure\Database;

Database::conectar();

require_once './routes/Api.php';

$app->run();
